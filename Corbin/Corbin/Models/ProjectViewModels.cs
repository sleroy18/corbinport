﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Corbin.Models
{
    public class ProjectViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        [Display(Name = "Last Updated")]
        public DateTime LastUpdated { get; set; }
    }

    public class ProjectCreateViewModel
    {
        [Required(ErrorMessage = "Please enter a {0}")]
        public string Title { get; set; }
        public string Description { get; set; }
        [Display(Name = "Upload Image")]
        public HttpPostedFileBase ImageFile { get; set; }
        [Display(Name = "Video Link")]
        public string VideoLink { get; set; }
    }

    public class ProjectDetailsViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime LastUpdated { get; set; }
        public IEnumerable<ProjectImage> Images { get; set; }
        public string VideoLink { get; set; }
    }

    public class ProjectEditViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter a {0}")]
        public string Title { get; set; }
        public string Description { get; set; }
        public IEnumerable<ProjectImage> Images { get; set; }
        public string VideoLink { get; set; }
        public 
    }

    //public class ProjectDeleteViewModel
    //{
    //    public int Id { get; set; }
    //    public string Title { get; set; }
    //    public byte[] imageStream { get; set; }
    //}

    //public class ImageViewModel
    //{
    //    public string Description { get; set; }
    //    public HttpPostedFileBase ImageFile { get; set; }
    //}

    //public class VideoViewModel
    //{
    //    public string Description { get; set; }
    //    public HttpPostedFileBase VideoFile { get; set; }
    //}
}