﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Corbin.Models;
using Microsoft.AspNet.Identity;

namespace Corbin.Controllers
{
    
    public class MyProjectsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: MyProjects
        public ActionResult Index()
        {
            var MyProjects = db.MyProjects.Include(p => p.User);
            List<ProjectViewModel> ProjectVMs = AutoMapper.Mapper.Map<List<MyProject>, List<ProjectViewModel>>(MyProjects.ToList());
            return View(ProjectVMs.ToList());
        }

        // GET: MyProjects/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MyProject proj = db.MyProjects.Find(id);
            ProjectDetailsViewModel ProjectVM = AutoMapper.Mapper.Map<MyProject, ProjectDetailsViewModel>(proj);
            ProjectVM.Images = db.ProjectImages.Where(u => u.ProjectId == proj.Id).ToList();

            if (proj == null)
            {
                return HttpNotFound();
            }
            return View(ProjectVM);
        }

        // GET: MyProjects/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.ApplicationUserId = new SelectList(db.Users, "Id", "Email");
            return View();
        }

        // POST: MyProjects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Title,Description,ImageFile")] ProjectCreateViewModel model)
        {
            // Initialization.  
            string fileContent = string.Empty;
            string fileContentType = string.Empty;

            try
            {

                if (ModelState.IsValid)
                {
                    MyProject proj = AutoMapper.Mapper.Map<ProjectCreateViewModel, MyProject>(model);
                    proj.EntryDate = DateTime.Now;
                    proj.LastUpdated = DateTime.Now;
                    proj.ApplicationUserId = User.Identity.GetUserId();
                    db.MyProjects.Add(proj);
                    db.SaveChanges();
                    int test = proj.Id;
                    //return RedirectToAction("Index");
                



                    ProjectImage projImage = new ProjectImage();
                    // Converting to bytes.  
                    byte[] uploadedFile = new byte[model.ImageFile.InputStream.Length];
                    model.ImageFile.InputStream.Read(uploadedFile, 0, uploadedFile.Length);


                    // Initialization.  
                    fileContent = Convert.ToBase64String(uploadedFile);
                    fileContentType = model.ImageFile.ContentType;

                    projImage.ImageStream = uploadedFile;
                    projImage.IsMainImage = true;
                    projImage.ProjectId = proj.Id;
                    projImage.ContentType = fileContentType;
                    projImage.Name = model.ImageFile.FileName;

                    db.ProjectImages.Add(projImage);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }
            
            //ViewBag.ApplicationUserId = new SelectList(db.Users, "Id", "Email", projVM.ApplicationUserId);
            return View(model);
        }

        // GET: MyProjects/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MyProject proj = db.MyProjects.Find(id);
            ProjectEditViewModel ProjectVM = AutoMapper.Mapper.Map<MyProject, ProjectEditViewModel>(proj);
            ProjectVM.Images = db.ProjectImages.Where(u => u.ProjectId == proj.Id).ToList(); if (proj == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApplicationUserId = new SelectList(db.Users, "Id", "Email", proj.ApplicationUserId);
            return View(ProjectVM);
        }

        // POST: MyProjects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,EntryDate,LastUpdated,ApplicationUserId")] MyProject proj)
        {
            if (ModelState.IsValid)
            {
                db.Entry(proj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ApplicationUserId = new SelectList(db.Users, "Id", "Email", proj.ApplicationUserId);
            return View(proj);
        }

        // GET: MyProjects/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MyProject proj = db.MyProjects.Find(id);
            if (proj == null)
            {
                return HttpNotFound();
            }
            return View(proj);
        }

        // POST: MyProjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            MyProject proj = db.MyProjects.Find(id);
            db.MyProjects.Remove(proj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        public JsonResult GetAll()
        {
            var MyProjects = db.MyProjects.Include(p => p.User);
            List<ProjectViewModel> ProjectVMs = AutoMapper.Mapper.Map<List<MyProject>, List<ProjectViewModel>>(MyProjects.ToList());
            return Json(ProjectVMs.ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
