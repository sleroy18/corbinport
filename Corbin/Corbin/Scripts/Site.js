﻿
(function () {
    $('#sidebarCollapse').on('click', function () {
        $(this).toggleClass('active');
        $('#sidebar').toggleClass('active');
        $(".toggle").toggleClass("show hide");
    });
})();

function handleScroll() {
    var TopOfFooter = $('#footer').offset().top;
    var BottomOfWindow = $(window).scrollTop() + $(window).height();
    var intPos = TopOfFooter - BottomOfWindow;
    if (intPos < 0) {
        var newHeight = $(window).height() + intPos;
        $('#sidebar').height(newHeight);
    } else {
        $('#sidebar').height('100%');
    }
}

function checkOffset() {

    //if ($('#sidebar').offset().top + $('#sidebar').height() >= $('#footer').offset().top - 10) {
    //    $('#sidebar').css('position', 'absolute');
    //}
    //if ($(document).scrollTop() + window.innerHeight < $('#footer').offset().top) {
    //    $('#sidebar').css('position', 'fixed');
    //}
}
