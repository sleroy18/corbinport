
var gallery = document.querySelector('.gallery');
var galleryItems = document.querySelectorAll('.gallery-item');
var numOfItems = gallery.children.length;
var itemWidth = 23; // percent: as set in css

var featured = document.querySelector('.featured-item');

var leftBtn = document.querySelector('.move-btn.gal-left');
var rightBtn = document.querySelector('.move-btn.gal-right');
var leftInterval;
var rightInterval;

var scrollRate = 0.1;
var left;

var lastImageId;

var imageFile;

function selectItem(e) {
	if (e.target.classList.contains('active')) return;
	
    featured.style.backgroundImage = e.target.style.backgroundImage;
    featured.dataset.imageId = e.target.id;
	
	for (var i = 0; i < galleryItems.length; i++) {
		if (galleryItems[i].classList.contains('active'))
			galleryItems[i].classList.remove('active');
	}
	
	e.target.classList.add('active');
}

function manualSelectItem(e) {

    if (e.classList.contains('active')) return;

    featured.style.backgroundImage = e.style.backgroundImage;
    featured.dataset.imageId = e.id;
    console.log(e.id);

    for (var i = 0; i < galleryItems.length; i++) {
        if (galleryItems[i].classList.contains('active'))
            galleryItems[i].classList.remove('active');
    }

    e.classList.add('active');
}

function galleryWrapLeft() {
	var first = gallery.children[0];
	gallery.removeChild(first);
	gallery.style.left = -itemWidth + '%';
	gallery.appendChild(first);
	gallery.style.left = '0%';
}

function galleryWrapRight() {
	var last = gallery.children[gallery.children.length - 1];
	gallery.removeChild(last);
	gallery.insertBefore(last, gallery.children[0]);
	gallery.style.left = '-23%';
}

function moveLeft() {
	left = left || 0;

	leftInterval = setInterval(function() {
		gallery.style.left = left + '%';

		if (left > -itemWidth) {
			left -= scrollRate;
		} else {
			left = 0;
			galleryWrapLeft();
		}
	}, 1);
}

function moveRight() {
	//Make sure there is element to the leftd
	if (left > -itemWidth && left < 0) {
		left = left - itemWidth;
		
		var last = gallery.children[gallery.children.length - 1];
		gallery.removeChild(last);
		gallery.style.left = left + '%';
		gallery.insertBefore(last, gallery.children[0]);	
	}
	
	left = left || 0;

	leftInterval = setInterval(function() {
		gallery.style.left = left + '%';

		if (left < 0) {
			left += scrollRate;
		} else {
			left = -itemWidth;
			galleryWrapRight();
		}
	}, 1);
}

function stopMovement() {
	clearInterval(leftInterval);
	clearInterval(rightInterval);
}

leftBtn.addEventListener('mouseenter', moveLeft);
leftBtn.addEventListener('mouseleave', stopMovement);
rightBtn.addEventListener('mouseenter', moveRight);
rightBtn.addEventListener('mouseleave', stopMovement);


//Start this baby up
(function init() {
	//var images = [
	//	'https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/car.jpg',
	//	'https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/city.jpg',
	//	'https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/deer.jpg',
	//	'https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/flowers.jpg',
	//	'https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/food.jpg',
	//	'https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/guy.jpg',
	//	'https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/landscape.jpg',
	//	'https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/lips.jpg',
	//	'https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/night.jpg',
	//	'https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/table.jpg'
	//];
	
	////Set Initial Featured Image
	//featured.style.backgroundImage = 'url(' + images[0] + ')';
	
	//Set Images for Gallery and Add Event Listeners
    for (var i = 0; i < galleryItems.length; i++) {
        if (i === 0) {
            featured.style.backgroundImage = 'url("data:' + document.getElementById("content" + i).value + ";base64," + document.getElementById("baseImage" + i).value + '")';            
            featured.dataset.imageId = galleryItems[i].id;

        }
        if (i % 2 === 0) {
            galleryItems[i].style.backgroundImage = 'url("data:' + document.getElementById("content" + i).value + ";base64," + document.getElementById("baseImage" + i).value + '")';
        }
        else {
            galleryItems[i].style.backgroundImage = 'url(https://s3-us-west-2.amazonaws.com/forconcepting/800Wide50Quality/table.jpg)';

        }
        galleryItems[i].addEventListener('click', selectItem);

        lastImageId = i;

    }

    //$(".featured-item").mouseenter(function () {
    //    $(".del-btn").addClass("active");
    //});

    //$(".featured-item").mouseleaver(function () {
    //    $(".del-btn").removeClass("active");
    //});

    //$(document).on('mouseenter', '.featured-item', function () {
    //    $(".del-btn").show();
    //}).on('mouseleave', '.featured-item', function () {
    //    $(".del-btn").hide();
    //});

    $("#del-btn").on("click", function () {
        var imageId = featured.dataset.imageId;
        console.log(imageId);
        document.getElementById(imageId).parentElement.remove();

        galleryItems = document.querySelectorAll('.gallery-item');
        //featured.style.backgroundImage = 'url("data:' + document.getElementById("content" + i).value + ";base64," + document.getElementById("baseImage" + i).value + '")';            
        //featured.dataset.imageId = galleryItems[i].id;

        if (galleryItems.length === 0) {
            featured.style.backgroundImage = 'url("")';
        }
        else {
            manualSelectItem(galleryItems[0]);
        }
    });

    $("#addImage").on("click", function () {
        var imageWrapper = document.createElement("div");
        imageWrapper.classList.add("item-wrapper");
        var imageDiv = document.createElement("div");
        imageDiv.classList.add("gallery-item");
        imageDiv.classList.add("image-holder");
        imageDiv.classList.add("r-3-2");
        imageDiv.classList.add("transition");
        imageDiv.addEventListener('click', selectItem);
        imageDiv.id = "image" + (lastImageId + 1);
        imageDiv.dataset.imageId = "image" + (lastImageId + 1);

        imageDiv.style.backgroundImage = 'url("' + imageFile + '")';
        imageWrapper.appendChild(imageDiv);

        document.getElementById("gallery").appendChild(imageWrapper);

        galleryItems = document.querySelectorAll('.gallery-item');

        $('#exampleModal').modal('toggle');
    });

    

})();

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            imageFile = e.target.result;

            $('#blah')
                .attr('src', e.target.result)
                .width(150)
                .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    }
}