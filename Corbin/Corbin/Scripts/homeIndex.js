﻿var animation_elements;
var web_window;
var projectJSON = [];


(function () {
    var resWidth = screen.width;
    const lowerResMax = 440;
    const highMaxRes = 768;

    if (resWidth >= highMaxRes) {
        createLargeContainer();
    }
    else if (resWidth > lowerResMax) {
        createMediumContainer();
    }
    else {
        createSmallContainer();
    }

    if (!document.querySelector ||
        !('classList' in document.body)) {
        return false;
    }

    // Read necessary elements from the DOM once
    var box = document.querySelector('.carouselbox');
    var next = box.querySelectorAll('.next-btn');
    var prev = box.querySelector('.prev-btn');

    // Define the global counter, the items and the 
    // current item 
    var counter = 0;
    var items = box.querySelector("#projectContainer").querySelectorAll('.content li');
    var amount = items.length;
    var current = items[0];

    // hide all elements and apply the carousel styling
    box.classList.add('active-slide');

    // navigate through the carousel

    function navigate(direction) {

        // hide the old current list item 
        current.classList.remove('current');

        // calculate th new position
        counter = counter + direction;

        // if the previous one was chosen and the counter is less than 0 make the counter the last element,
        if (direction === -1 && counter < 0) {
            counter = amount - 1;
        }

        // if the next button was clicked and there is no items element, set the counter to 0
        if (direction === 1 && !items[counter]) {
            counter = 0;
        }

        // set new current element 
        current = items[counter];
        current.classList.add('current');
    }

    //add event handlers to buttons
    next.forEach(function (e) { 
        e.addEventListener('click', function (ev) {
            navigate(1);
        });
    });
    prev.addEventListener('click', function (ev) {
        navigate(-1);
    });

    navigate(0);

    var timer = null;

    window.onresize = function () {
        if (timer) {
            clearTimeout(timer);   // clear any previous pending timer
        }
        timer = setTimeout(function () {
            if (screen.width >= highMaxRes && resWidth < highMaxRes) {
                clearContainer("projectContainer");
                createLargeContainer();
                items = box.querySelector("#projectContainer").querySelectorAll('.content li');
                current = items[0];
                current.classList.add('current');
                counter = 0;
                resWidth = screen.width;
            }
            else if (screen.width < highMaxRes && screen.width > lowerResMax) {
                if (resWidth >= highMaxRes || resWidth < lowerResMax) {
                    clearContainer("projectContainer");
                    createMediumContainer();
                    items = box.querySelector("#projectContainer").querySelectorAll('.content li');
                    current = items[0];
                    current.classList.add('current');
                    counter = 0;
                    resWidth = screen.width;
                }
            }
            else if (screen.width < lowerResMax && resWidth > lowerResMax) {
                clearContainer("projectContainer");
                createSmallContainer();
                items = box.querySelector("#projectContainer").querySelectorAll('.content li');
                current = items[0];
                current.classList.add('current');
                counter = 0;
                resWidth = screen.width;
            }          
        }, 50);  
    }
})();

function generateCard(title, img) {
    var cardOuter = document.createElement("Div");
    cardOuter.classList.add("col");
    cardOuter.classList.add("cardOuter");
    console.log("url(" + img + ")");
    cardOuter.style.backgroundImage = "url(" + img + ")";

    var cardInner = document.createElement("Div");
    cardInner.classList.add("cardInner");
    cardInner.innerText = title;
    cardOuter.appendChild(cardInner);

    return cardOuter;
}

function createLargeContainer() {
    var count = 0;
    var li;
    var div;
    for (var i = 0; i < 6; i++) {

        if (i % 3 === 0) {
            count++;
            li = document.createElement("li");
            li.id = "liItem" + count;

            div = document.createElement("div");
            div.id = "divItem" + count;
            div.classList.add("row");
            li.appendChild(div);
        }

        var card = generateCard("Test: " + i, "/Content/Images/feeder.jpg");
        div.appendChild(card);

        if ((i + 1) % 3 === 0) {
            document.getElementById("projectContainer").appendChild(li);
        }
    }
}

function createMediumContainer() {
    var count = 0;
    var li;
    var div;
    for (var i = 0; i < 6; i++) {

        if (i % 2 === 0) {
            count++;
            li = document.createElement("li");
            li.id = "liItem" + count;

            div = document.createElement("div");
            div.id = "divItem" + count;
            div.classList.add("row");
            li.appendChild(div);
        }

        var card = generateCard("Test: " + i, "/Content/Images/feeder.jpg");
        div.appendChild(card);

        if ((i + 1) % 2 === 0) {
            document.getElementById("projectContainer").appendChild(li);
        }
    }
}

function createSmallContainer() {
    var count = 0;
    var li;
    var div;
    for (var i = 0; i < 6; i++) {
        count++;
        li = document.createElement("li");
        li.id = "liItem" + count;

        div = document.createElement("div");
        div.id = "divItem" + count;
        div.classList.add("row");
        li.appendChild(div);

        var card = generateCard("Test: " + count, "/Content/Images/feeder.jpg");
        div.appendChild(card);

        document.getElementById("projectContainer").appendChild(li);
    }
}

function clearContainer(name) {
    var container = document.getElementById(name);
    for (let i = container.childNodes.length - 1; i >= 0; i--) {
        container.removeChild(container.childNodes[i]);
    }
}