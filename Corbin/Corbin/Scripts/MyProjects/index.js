﻿$(document).ready(function () {
    $.ajax({
        dataType: 'json',
        url: "/MyProjects/GetAll"
    }).then(function (data) {
        $.each(data, function (k) {
            var date = new Date(parseInt(data[k].LastUpdated.split("(")[1].split(")")[0].trim()));
            data[k].LastUpdated = date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getUTCFullYear();
            if (data[k].Description.length > 25) {
                data[k].Description = data[k].Description.substring(0, 25) + "...";
            }
        });
        $('#myProjectsTable').DataTable({
            scrollY: 400,
            "columnDefs": [
                { "width": "100px", "targets": "_all" }
            ],
            data: data,
            columns: [
                { title: "Title", data: "Title" },
                { title: "Description", data: "Description" },

                { title: "Last Updated", data: "LastUpdated" }
            ]
        });

        $('#myProjectsTable tbody').on('click', 'tr', function () {
            var table = $('#myProjectsTable').DataTable();
            window.location.href = "/MyProjects/Details/" + table.row(this).data().Id;
        });

    }); 
});