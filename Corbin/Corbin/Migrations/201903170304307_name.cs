namespace Corbin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class name : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectImages", "Name", c => c.String());
            DropColumn("dbo.ProjectImages", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectImages", "Description", c => c.String());
            DropColumn("dbo.ProjectImages", "Name");
        }
    }
}
