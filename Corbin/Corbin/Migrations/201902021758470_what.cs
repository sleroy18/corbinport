namespace Corbin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class what : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RegisterEmails", "Email", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RegisterEmails", "Email", c => c.String());
        }
    }
}
