namespace Corbin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class setup : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.RegisterEmails", new[] { "Email" });
            AlterColumn("dbo.RegisterEmails", "Email", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RegisterEmails", "Email", c => c.String(maxLength: 200));
            CreateIndex("dbo.RegisterEmails", "Email", unique: true);
        }
    }
}
