namespace Corbin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class contentType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectImages", "ContentType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectImages", "ContentType");
        }
    }
}
